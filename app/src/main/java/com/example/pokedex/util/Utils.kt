package com.example.pokedex.util

import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.ExperimentalUnitApi
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import androidx.compose.ui.unit.dp

enum class Destinations(val route: String) {
    TYPE("type"), POKEMON_IN_TYPE("pokemon_in_type"), POKEMON("pokemon")
}

@OptIn(ExperimentalUnitApi::class)
@Composable
fun PokeTextView(text: String) {
    Text(
        text = text,
        color = MaterialTheme.colorScheme.onSurface,
        fontSize = TextUnit(24f, TextUnitType.Sp),
        modifier = Modifier.padding(8.dp),
        textAlign = TextAlign.Center
    )
}

fun String.capitalizeFirst(): String {
    val first = this.first()
    val theRest = this.substring(1)
    return first.uppercase() + theRest
}

@Preview
@Composable
fun PokePreview() {
    PokeTextView(text = "Charmander")
}