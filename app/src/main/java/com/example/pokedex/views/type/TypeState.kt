package com.example.pokedex.views.type

import com.example.commonlib.model.remote.dto.typeEndpoint.TypeResponse

data class TypeState(
    val isLoading: Boolean = false,
    val error: Throwable? = null,
    val types: List<TypeResponse.PokemonType>? = listOf()
)