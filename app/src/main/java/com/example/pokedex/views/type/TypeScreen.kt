package com.example.pokedex.views.type

import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.example.commonlib.model.remote.dto.typeEndpoint.TypeResponse
import com.example.pokedex.util.PokeTextView
import com.example.pokedex.util.capitalizeFirst

@Composable
fun TypeScreen(
    typeState: TypeState?,
    onDialogExit: () -> Unit,
    onTypeCick: (type: String) -> Unit
) {
    when {
        typeState?.isLoading == true -> {
            CircularProgressIndicator()
        }
        typeState?.error != null -> {
            AlertDialog(
                onDismissRequest = {
                    onDialogExit.invoke()
                },
                title = { Text(text = "Oh no, something went wrong?!!?!!?!") },
                text = { Text(text = typeState.error.localizedMessage ?: "") },
                confirmButton = {
                    Button(onClick = {
                        onDialogExit.invoke()
                    }) {
                        Text(text = "Fine, but I'm watching you!!")
                    }
                }
            )
        }
        !typeState?.types.isNullOrEmpty() -> {
            typeState?.types?.let {
                DisplayListInGrid(it, onTypeCick)
            }
        }
        else -> {
            //Apologize for failing
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DisplayListInGrid(
    types: List<TypeResponse.PokemonType>,
    onTypeCick: (type: String) -> Unit
) {

    Column(
        verticalArrangement = Arrangement.SpaceAround,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        PokeTextView(text = "Pokemon Types")
        LazyVerticalGrid(
            columns = GridCells.Adaptive(150.dp),
            verticalArrangement = Arrangement.Center,
            modifier = Modifier.padding(8.dp)
        ) {
            items(types.size) { index ->
                val item = types[index]
                Card(
                    modifier = Modifier
                        .padding(16.dp)
                        .border(2.dp, Color.Black, RoundedCornerShape(8.dp)),
                    onClick = {
                        //When a type of pokemon is selected.

                    },
                ) {
                    PokeTextView(text = item.name.capitalizeFirst())
                }
            }
        }
    }
}
