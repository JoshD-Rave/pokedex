package com.example.pokedex.views

import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.lifecycleScope
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.commonlib.usecases.GetTypeUseCase
import com.example.pokedex.ui.theme.PokeDexTheme
import com.example.pokedex.util.Destinations
import com.example.pokedex.viewModels.PokeVM
import com.example.pokedex.viewModels.PokeVMFactory
import com.example.pokedex.views.type.TypeScreen

class MainActivity : ComponentActivity() {
    private val TAG = "MainActivity"

    private lateinit var vmFactory: PokeVMFactory
    private val pokeVM by viewModels<PokeVM> { vmFactory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vmFactory = PokeVMFactory(GetTypeUseCase())
        setContent {
            PokeDexTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    lifecycleScope.launchWhenCreated {
                        pokeVM.getPokemonTypes()
                    }
                    val navController = rememberNavController()
                    NavHost(
                        navController = navController,
                        startDestination = Destinations.TYPE.route
                    ) {
                        composable(route = Destinations.TYPE.route) {
                            TypeScreen(
                                typeState = pokeVM.typeList.value,
                                onDialogExit = {
                                    Toast.makeText(
                                        this@MainActivity,
                                        "Alert dialog dismissed",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }) { chosenType ->
                                lifecycleScope.launchWhenResumed {
                                    pokeVM.getPokemonInType(chosenType)
                                }
                            }
                        }
                        composable("Never") {
                            Greeting("Android")
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    PokeDexTheme {
        Greeting("Android")
    }
}