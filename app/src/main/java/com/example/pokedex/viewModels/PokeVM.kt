package com.example.pokedex.viewModels

import android.util.Log
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import com.example.commonlib.usecases.GetTypeUseCase
import com.example.pokedex.views.type.TypeState

class PokeVM(
    val getTypes: GetTypeUseCase
) : ViewModel() {
    private val TAG = "PokeVM"

    val typeList = mutableStateOf(TypeState())

    suspend fun getPokemonTypes() {
        typeList.value = typeList.value.copy(isLoading = true)
        val theList = getTypes(30)
        Log.e(TAG, "getPokemonTypes: the list was $theList")
        typeList.value = typeList.value.copy(
            isLoading = false,
            types = theList.getOrNull(),
            error = theList.exceptionOrNull()
        )
    }

    suspend fun getPokemonInType(type: String) {
        Log.e(TAG, "getPokemonInType: going to get Pokemon of type $type")
    }

}