package com.example.pokedex.viewModels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.commonlib.usecases.GetTypeUseCase

class PokeVMFactory(
    private val getTypeUC: GetTypeUseCase
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return PokeVM(getTypeUC) as T
    }
}