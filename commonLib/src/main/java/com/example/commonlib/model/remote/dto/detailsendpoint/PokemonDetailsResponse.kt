package com.example.commonlib.model.remote.dto.detailsendpoint

data class PokemonDetailsResponse(
    val base_experience: Int,
    val forms: List<Summary>,
    val height: Int,
    val id: Int,
    val is_default: Boolean,
    val moves: List<Move>,
    val name: String,
    val order: Int,
    val species: Summary,
    val sprites: Sprites,
    val types: List<Type>,
    val weight: Int
)