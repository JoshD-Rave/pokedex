package com.example.commonlib.model.remote.dto.typeEndpoint

import com.google.gson.annotations.SerializedName

data class TypeResponse(
    val count: Int,
    val next: String?,
    val previous: String?,
    @SerializedName("results")
    val pokemonTypes: List<PokemonType>
) {
    data class PokemonType(
        val name: String,
        val url: String
    )
}