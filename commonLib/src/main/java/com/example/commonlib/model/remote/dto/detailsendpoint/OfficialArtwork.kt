package com.example.commonlib.model.remote.dto.detailsendpoint

data class OfficialArtwork(
    val front_default: String
)