package com.example.commonlib.model.remote.dto.detailsendpoint

import com.google.gson.annotations.SerializedName

data class Other(
    @SerializedName("official-artwork")
    val officialArtwork: OfficialArtwork
)