package com.example.commonlib.model.remote.services

import com.example.commonlib.model.remote.dto.detailsendpoint.PokemonDetailsResponse
import com.example.commonlib.model.remote.dto.listendpoint.PokemonListResponse
import com.example.commonlib.model.remote.dto.typeEndpoint.TypeResponse
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PokemonService {
    companion object {
        fun getInstance(): PokemonService = Retrofit.getInstance().create()
    }

    @GET(Retrofit.POKEMON_TYPE_ENDPOINT)
    suspend fun getPokemonTypes(@Query("limit") limit: Int): TypeResponse

    @GET("{type}")
    suspend fun getPokemonOfType(@Path("type") typeEndpoint: String): PokemonListResponse

    @GET("{pokemon}")
    suspend fun getPokemon(@Path("pokemon") pokemonURL: String): PokemonDetailsResponse

}