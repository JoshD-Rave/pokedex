package com.example.commonlib.model.remote.dto.listendpoint

data class PokemonSummary(
    val name: String,
    val url: String
)