package com.example.commonlib.model.remote.dto.detailsendpoint

data class Type(
    val slot: Int,
    val type: Summary
)