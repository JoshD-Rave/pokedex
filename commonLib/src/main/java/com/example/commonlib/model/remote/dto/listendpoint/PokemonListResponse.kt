package com.example.commonlib.model.remote.dto.listendpoint

data class PokemonListResponse(
    val id: Int,
    val moves: List<Move>,
    val name: String,
    val pokemon: List<Pokemon>
) {
    data class Move(
        val name: String,
        val url: String
    )

    data class Pokemon(
        val pokemon: PokemonSummary,
        val slot: Int
    )
}