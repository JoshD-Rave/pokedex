package com.example.commonlib.model.remote.dto.detailsendpoint

data class Summary(
    val name: String,
    val url: String
)