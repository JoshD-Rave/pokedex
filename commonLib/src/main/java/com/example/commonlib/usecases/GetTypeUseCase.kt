package com.example.commonlib.usecases

import android.util.Log
import com.example.commonlib.PokemonRepo
import com.example.commonlib.model.remote.dto.typeEndpoint.TypeResponse

class GetTypeUseCase() {
    private val TAG: String = "GetTypeUseCase"

    suspend operator fun invoke(limit: Int = 20): Result<List<TypeResponse.PokemonType>> {
        return try {
            val result = PokemonRepo.getPokemonTypes(limit)
            Log.e(TAG, "invoke: ")
            Result.success(result.pokemonTypes)
        } catch (ex: Exception) {
            Result.failure(ex)
        }
    }
}