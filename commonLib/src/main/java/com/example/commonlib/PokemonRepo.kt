package com.example.commonlib

import android.util.Log
import com.example.commonlib.model.remote.services.PokemonService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object PokemonRepo {
    private const val TAG = "PokemonRepo"
    private val pokeService by lazy {
        PokemonService.getInstance()
    }

    suspend fun getPokemonTypes(limit: Int) = withContext(Dispatchers.IO) {
        val pokeTypes = pokeService.getPokemonTypes(limit) // Default 20 gotten
        Log.e(TAG, "getPokemonTypes: $pokeTypes")
        pokeTypes
    }

    suspend fun getPokemonFromURL(url: String) = withContext(Dispatchers.IO) {
        val theURl = breakDownUrl(url)
        pokeService.getPokemonOfType(theURl)

    }

    private fun breakDownUrl(url: String): String {
        return ""
    }

}